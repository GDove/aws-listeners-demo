package com.reagroup.awslistenersdemo.model;

import java.util.Objects;

public class Loan {
    private String id;
    private String body;

    public String getId() {
        return id;
    }

    public Loan setId(String id) {
        this.id = id;
        return this;
    }

    public String getBody() {
        return body;
    }

    public Loan setBody(String body) {
        this.body = body;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Loan loan = (Loan) o;
        return Objects.equals(id, loan.id) &&
                Objects.equals(body, loan.body);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, body);
    }

    @Override
    public String toString() {
        return "Loan{" +
                "id='" + id + '\'' +
                ", body='" + body + '\'' +
                '}';
    }
}
