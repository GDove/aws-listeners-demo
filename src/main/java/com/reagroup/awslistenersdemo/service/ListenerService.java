package com.reagroup.awslistenersdemo.service;

import com.reagroup.awslistenersdemo.model.Loan;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.aws.messaging.config.annotation.NotificationMessage;
import org.springframework.cloud.aws.messaging.listener.Acknowledgment;
import org.springframework.cloud.aws.messaging.listener.SqsMessageDeletionPolicy;
import org.springframework.cloud.aws.messaging.listener.annotation.SqsListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class ListenerService {

    @SqsListener(value = "queue1", deletionPolicy = SqsMessageDeletionPolicy.NEVER)
    public void subscriberSNSSQS(
            @NotificationMessage Loan loan,
            @Payload Object payload, // TODO create SNS Payload object
            Acknowledgment acknowledgment
    ) {
        log.info("Thread name - " + Thread.currentThread().getName());
        log.info("Queue1 SQS Payload - " + payload.toString());
        log.info("Queue1 SNS Payload - " + loan.toString());
        acknowledgment.acknowledge();
    }

    @SqsListener(value = "queue2", deletionPolicy = SqsMessageDeletionPolicy.NEVER)
    public void subscriberSQS(
            @Payload Loan loan,
            Acknowledgment acknowledgment
    ) {
        log.info("Thread name - " + Thread.currentThread().getName());
        log.info("Queue2 Payload - "+ loan.toString());
        acknowledgment.acknowledge();
    }
}
