package com.reagroup.awslistenersdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AwsListenersDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(AwsListenersDemoApplication.class, args);
    }

}
