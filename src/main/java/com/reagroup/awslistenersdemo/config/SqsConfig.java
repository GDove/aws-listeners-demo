package com.reagroup.awslistenersdemo.config;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.EnvironmentVariableCredentialsProvider;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.sqs.AmazonSQSAsync;
import com.amazonaws.services.sqs.AmazonSQSAsyncClientBuilder;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.cloud.aws.messaging.config.QueueMessageHandlerFactory;
import org.springframework.cloud.aws.messaging.config.annotation.EnableSqs;
import org.springframework.cloud.aws.messaging.core.QueueMessagingTemplate;
import org.springframework.cloud.aws.messaging.listener.QueueMessageHandler;
import org.springframework.cloud.aws.messaging.listener.SimpleMessageListenerContainer;
import org.springframework.cloud.aws.messaging.support.NotificationMessageArgumentResolver;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.task.AsyncTaskExecutor;
import org.springframework.messaging.converter.MappingJackson2MessageConverter;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.List;

@EnableSqs
@Configuration
public class SqsConfig {

    @Bean
    @Primary
    public AmazonSQSAsync sqsClient() {
        return AmazonSQSAsyncClientBuilder.standard()
                .withRegion(Regions.AP_SOUTHEAST_2)
                .withCredentials(getCredentialsProvider())
                .build();
    }

    public AWSCredentialsProvider getCredentialsProvider() {
//        return new EnvironmentVariableCredentialsProvider();
        return new ProfileCredentialsProvider("messaging");
    }

    @Bean
    public QueueMessagingTemplate getMessageTemplate() {
        return new QueueMessagingTemplate(sqsClient());
    }

    @Bean
    public QueueMessageHandlerFactory queueMessageHandlerFactory(AmazonSQSAsync amazonSQS,
                                                                 BeanFactory beanFactory,
                                                                 ObjectMapper objectMapper) {

        QueueMessageHandlerFactory factory = new QueueMessageHandlerFactory();
        factory.setAmazonSqs(amazonSQS);
        factory.setBeanFactory(beanFactory);

        MappingJackson2MessageConverter mappingJackson2MessageConverter = new MappingJackson2MessageConverter();
        mappingJackson2MessageConverter.setSerializedPayloadClass(String.class);
        mappingJackson2MessageConverter.setObjectMapper(objectMapper);
        mappingJackson2MessageConverter.setStrictContentTypeMatch(false); // extra fields in a payload will be ignored

        // sqs payload converters
        factory.setMessageConverters(List.of(mappingJackson2MessageConverter));

        // NotificationMsgArgResolver is used to deserialize the “Message” data from SNS Notification
        factory.setArgumentResolvers(List.of(
                new NotificationMessageArgumentResolver(
                        mappingJackson2MessageConverter)));

        return factory;
    }

    @Bean
    public SimpleMessageListenerContainer simpleMessageListenerContainer(
            AmazonSQSAsync amazonSQSAsync, QueueMessageHandler queueMessageHandler) {
        SimpleMessageListenerContainer simpleMessageListenerContainer = new SimpleMessageListenerContainer();
        simpleMessageListenerContainer.setAmazonSqs(amazonSQSAsync);
        simpleMessageListenerContainer.setMessageHandler(queueMessageHandler);
        simpleMessageListenerContainer.setMaxNumberOfMessages(10);
        simpleMessageListenerContainer.setTaskExecutor(threadPoolTaskExecutor());
        return simpleMessageListenerContainer;
    }

    @Bean
    public AsyncTaskExecutor threadPoolTaskExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setThreadNamePrefix("Sqs subscribers - thread N");
        executor.setCorePoolSize(10); // Max number of thread shared by all queues (as there is only one handler)
        executor.setThreadPriority(Thread.MIN_PRIORITY);
        return executor;
    }
}
