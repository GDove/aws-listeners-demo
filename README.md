
# Getting Started

Demo application for Spring Cloud AWS.

App is listening to 2 SQS queues "queue1" and "queue2".
queue1 is subscribed to SNS topic1.
queue2 is a reference stand alone queue.  

Use AWS UI to post messages to SNS ans SQS.

## Prerequisites
1. AWS account
1. Admin access to the account
1. AWS CLI installed on your machine
1. ~/.aws/credentials configured to use admin profile
   ```
   [admin]
   aws_access_key_id = <admin_key>
   aws_secret_access_key = <admin_secret_key>
   region=ap-southeast-2
   
   ... other profiles     

1. Ruby and gem installed locally

## Setup
1. install REA stackup tool https://github.com/realestate-com-au/stackup
   
    `gem install stackup`
1. Use stackup tool to provision required resources to your cloud

    `sh auto/deployStack.sh`
1. Go to the AWS IAM, find `demoUser`, generate new access key in Security and credentials tab. 
1. Add generated access key to aws credentials file `~/.aws/credentials` under `messaging` profile.
    ```
   [admin]
   aws_access_key_id = <admin_key>
   aws_secret_access_key = <admin_secret_key>
   region=ap-southeast-2
   [messaging]
   aws_access_key_id = <demoUser_key>
   aws_secret_access_key = <demoUser_secret_key>
   region=ap-southeast-2
   
    ... other profiles
   ```
1. run application 
    `sh auto/run.sh`

Congrats you're ready to start posting messages.
```
{
    "id": "123",
    "body": "Loan body"  
}
```
1. when you're done delete the AWS stack
    `sh auto/deleteStack.sh`

### Reference Documentation
For further reference, please consider the following sections:

* [Official Apache Maven documentation](https://maven.apache.org/guides/index.html)
* [Spring Boot Maven Plugin Reference Guide](https://docs.spring.io/spring-boot/docs/2.3.2.RELEASE/maven-plugin/reference/html/)
* [Create an OCI image](https://docs.spring.io/spring-boot/docs/2.3.2.RELEASE/maven-plugin/reference/html/#build-image)

### Miscellaneous

When SQS subscribed to a topic payload of the sqs message will look like:
```
{
    "Type" : "Notification",
    "MessageId" : "f5a3c571-1ef2-5be6-a957-9df8b0bd2524",
    "TopicArn" : "arn:aws:sns:ap-southeast-2:068276765234:topic1",
    "Message" : "{\n  \"id\": \"123\",\n  \"body\": \"super pumped body\"  \n}",
    "Timestamp" : "2020-01-26T05:43:43.373Z",
    "SignatureVersion" : "1",
    "Signature" : "LbNR2fP+dN93M4k8g4zXB6RBsZjjSNj4kouObKoKGrIe1drEL5uonWMJM3sqo1bK1kU4IyqmEmdMqg5pO2nLvhr3tot99WKu/gP88XKuksFxJpx2MCwSSQ7gA/89ZdNwsR4IgXLmqIW5GyJyrtXG5ta18+Qz7BqPEH1HZxlnPKW4EVIw==",
    "SigningCertURL" : "https://sns.ap-southeast-2.amazonaws.com/SimpleNotificationService-010a507c183cd94bdbd83a.pem",
    "UnsubscribeURL" : "https://sns.ap-southeast-2.amazonaws.com/?Action=Unsubscribe&SubscriptionArn=arn:aws:sns:ap-southeast-2:0682768449:topic1:ead98e19-05a3-4ccf-086c50"
}
```
